
import globVar
import logging
import logging.config
import json
import os
import subprocess

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

RRU_STATUS_FILE = globVar.OUTPUT_DIR + os.path.sep + "erbs_rru_status.json"
RRU_FDN_FILE = globVar.ERBS_FDN_FILE
RRU_STATUS_INIT_TEMP_FILE = globVar.ERBS_RRU_INIT_TEMP_FILE

def init():
    logger.info("start initializing erbs rru status")

    if os.path.isfile(RRU_FDN_FILE):
        try:
            logger.info("read {file}".format(file=RRU_FDN_FILE))
            with open(RRU_FDN_FILE, "r") as f:
                fdns = json.load(f)
            for i in range(len(fdns)):
                fdn = fdns[i]
                if i == 0:
                    echoCmd = "echo {fdn} > {tmpFile}".format(fdn=fdn, tmpFile=RRU_STATUS_INIT_TEMP_FILE)
                else:
                    echoCmd = "echo {fdn} >> {tmpFile}".format(fdn=fdn, tmpFile=RRU_STATUS_INIT_TEMP_FILE)
                #logger.debug("run {cmd}".format(cmd=echoCmd))
                subprocess.call(echoCmd, shell=True)
                cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS la {fdn} | egrep \"operationalState|administrativeState\" >> {tmpFile}".format(fdn=fdn, tmpFile=RRU_STATUS_INIT_TEMP_FILE)
                #logger.debug("run {cmd}".format(cmd=cmd))
                try:
                    subprocess.call(cmd, shell=True)
                except Exception, e:
                    logger.warning(e)
        except Exception, e:
                logger.error(e)
    else:
        logger.error("missing rru fdn file {file}".format(file=RRU_FDN_FILE))

    logger.info("end initializing erbs rru status")

if __name__ == "__main__":
    init()