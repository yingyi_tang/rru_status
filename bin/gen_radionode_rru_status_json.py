import globVar
import subprocess
import json
import logging
import logging.config

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("RadionodeController")
except Exception, e:
    print(e)

RRU_STATUS_TEMP_FILE = globVar.RADIONODE_RRU_STATUS_TEMP_FILE
RRU_STATUS_FILE = globVar.RADIONODE_RRU_STATUS_FILE

def gen():
    """
     radionode_rru_status_init.json
    /opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > radionode_rru_status_init.tmp
    :return:
    """
    # Generate radionode_rru_status_init.tmp
    gen_radinode_rru_status_ini_tmp_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > {file}".format(file=RRU_STATUS_TEMP_FILE)
    logger.info("run {cmd}".format(cmd=gen_radinode_rru_status_ini_tmp_cmd))
    subprocess.call(gen_radinode_rru_status_ini_tmp_cmd, shell=True)
    logger.info("generated {file}".format(file=RRU_STATUS_TEMP_FILE))
    # Generate radionode_rru_status_init.json
    statusDict = {}
    rru = ads = ops = None
    rruNum = 0
    with open(RRU_STATUS_TEMP_FILE, "r") as f:
        lines = f.readlines()
    logger.info("read {file}".format(file=RRU_STATUS_TEMP_FILE))
    for line in lines:
        if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
            rru = line.lstrip().rstrip("\n")
        elif line.lstrip().startswith("[1] administrativeState"):
            ads = line.split(":")[1].lstrip().rstrip("\n")
        elif line.lstrip().startswith("[2] operationalState"):
            ops = line.split(":")[1].lstrip().rstrip("\n")
        else:
            pass
        if rru and ads and ops:
            rruNum += 1
            statusDict[rru] = [{"administrativeState": ads, "operationalState": ops}]
            rru = ads = ops = None
    logger.info("found total radionode RRU number: {rruNum}".format(rruNum=rruNum))
    #print(statusDict)

    with open(RRU_STATUS_FILE, "w") as f1:
        json.dump(statusDict, f1, indent=1, sort_keys=True)
        logger.info("generated {file}".format(file=RRU_STATUS_FILE))

if __name__ == "__main__":
    gen()