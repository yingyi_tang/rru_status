import datetime
import commands
import globVar
import logging
import logging.config
import json
import os
import subprocess

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

DELTA = -5
FILTER_FILE = globVar.ERBS_RRU_ALARM_FILTER_FILE
als_tmp_file = globVar.TEMP_DIR + os.path.sep + "alarmlog.tmp"

def alarm_search():
    # generate filter file
    now = datetime.datetime.now()
    cur_date = now.strftime("%m/%d/%Y")
    end_time = now.strftime("%H:%M:%S")
    delta = datetime.timedelta(minutes=DELTA)
    start_time = (now + delta).strftime("%H:%M:%S")
    _,domainname = commands.getstatusoutput('domainname')
    all_attrs = ["ack", "ackt", "ai", "aid", "bobj", "bstat", "cat", "class", "com", \
        "cr", "ct", "ct", "ctype", "dd", "fmxg", "id", "num", "ocr", "oor", \
        "ot", "pc", "pd", "pra", "ps", "pt", "rt", "sp", "time", "trend", "type"]
    with open(FILTER_FILE, "w") as filter_fh:
        filter_fh.write("-oor\t=\t\to\n")
        filter_fh.write("SubNetwork=ONRM_ROOT_MO*\n")
        filter_fh.write("-ltime\tbetween\t\to\n")
        filter_fh.write("%s\n" %(cur_date))
        filter_fh.write("%s\n" %(start_time))
        filter_fh.write("%s\n" %(cur_date))
        filter_fh.write("%s\n" %(end_time))
        for attr in all_attrs:
            filter_fh.write("-%s\t=\t\to\n" %(attr))
        filter_fh.write("-sort\n")
        filter_fh.write("ltime\tFORWARD\n")
    logger.info("generated {file}".format(file=FILTER_FILE))

    cmd = "/opt/ericsson/bin/als_search -read {filter} -mib {dname} -olf {outfile}".format(filter=globVar.ERBS_RRU_ALARM_FILTER_FILE, dname=domainname, outfile=als_tmp_file)
    try:
        logger.info("run {cmd}".format(cmd=cmd))
        subprocess.call(cmd, shell=True)
        logger.info("generated {file}".format(file=als_tmp_file))
    except Exception, e:
        logger.error(e)

def gen():
    managed_erbs_name = []
    with open(globVar.ERBS_NAME_FILE, "r") as f:
        managed_erbs_name = json.load(f)

    alarm_info_filter = []
    with open(globVar.ERBS_RRU_ALARM_INFO_FILTER_FILE, "r") as f:
        alarm_info_filter = json.load(f)

    # find out which erbs need amos update
    logger.info("read {file}".format(file=als_tmp_file))
    with open(als_tmp_file, "r") as f:
        lines = f.readlines()
    erbs_update_list = []
    for i in range(len(lines)):
        try:
            if lines[i].startswith("@ID="):
                # find MeContext ERBS
                mm = lines[i].split("@")[7].split(",")[2]   # ManagedElement=GZZCGuangYuanLiXinGS-ENH or MeContext=GZZCZengChengSuiDongJLZE-ELW
                sp = lines[i].split("@")[9].lstrip("SP=")
                if mm.startswith("MeContext=") and sp in alarm_info_filter:   # filter alarm
                    #logger.debug("find alarm: {alarm}".format(alarm=lines[i].strip("\n")))
                    erbs_name = mm.split("=")[1]
                    if erbs_name not in erbs_update_list and erbs_name in managed_erbs_name:  # ERBS is managed
                        erbs_update_list.append(erbs_name)
            else:
                pass
        except Exception, e:
            logger.error(e)
    logger.info("find total {num} managed erbs rru status need update".format(num=len(erbs_update_list)))
    logger.debug("erbs update list {list}".format(list=erbs_update_list))
    #print(count)

    if len(erbs_update_list) > 0:
        erbs_name_amosbatch_file = globVar.TEMP_DIR + os.path.sep + ".erbs_name_alarm_amosbatch_file.txt"
        with open(erbs_name_amosbatch_file, "w") as f:
            f.truncate()
            content = "\n".join(erbs_update_list)
            f.writelines(content)
        logger.info("generated {file}".format(file=erbs_name_amosbatch_file))
        get_rru_status_cli = "amosbatch -p 1 {file} 'lt all; get AuxPluginUnit' {dir}".format(file=erbs_name_amosbatch_file, dir=globVar.ALARM_AMOSBATCH_TEMP_DIR)
        subprocess.call(get_rru_status_cli, shell=True)
        logger.info("run {cmd}".format(cmd=get_rru_status_cli))

        # parse amosbatch output file to get rru status
        firstHalfOfFdn = restHalfOfFdn = ads = ops = None
        statusDict = {}
        for i in range(len(erbs_update_list)):
            try:
                amosLogFile = globVar.ALARM_AMOSBATCH_TEMP_DIR + os.path.sep + erbs_update_list[i] + ".log"
                logger.info("read {file}".format(file=amosLogFile))
                with open(amosLogFile, "r") as f:
                    lines = f.readlines()
                for k in range(len(lines)):
                    if "SubNetwork=ONRM_ROOT_MO_R" in lines[k] and lines[k].startswith("Connected to"):
                        # example: SubNetwork=ONRM_ROOT_MO_R,SubNetwork=GZKFLTE01,MeContext=GZKFChangLongCunD-ELH,ManagedElement=1
                        firstHalfOfFdn = lines[k].split()[3].strip("()")
                        #print(firstHalfOfFdn)
                    elif "Equipment=1,AuxPlugInUnit=" in lines[k]:
                        # example: Equipment=1,AuxPlugInUnit=RRU-3
                        restHalfOfFdn = lines[k].split()[1]
                        #print(restHalfOfFdn)
                    elif lines[k].startswith("administrativeState"):
                        ads = lines[k].split()[1]
                        #print(ads)
                    elif lines[k].startswith("operationalState"):
                        ops = lines[k].split()[1]
                        #print(ops)
                    else:
                        pass

                    if firstHalfOfFdn and restHalfOfFdn and ads and ops:
                        #print(firstHalfOfFdn + "," + restHalfOfFdn + "_" + ads + "_" + ops)
                        rru = firstHalfOfFdn + "," + restHalfOfFdn
                        statusDict[rru] = [{"administrativeState": ads, "operationalState": ops}]
                        #print(statusDict)
                        restHalfOfFdn = ads = ops = None
                        #print("yes")
                os.remove(amosLogFile)
                #logger.debug("notification update status {status}".format(status=statusDict))
            except Exception,e:
                logger.error(e)

        # merge status to notification update file
        old_status = {}
        if os.path.isfile(globVar.ERBS_RRU_ALARM_FILE):
            with open(globVar.ERBS_RRU_ALARM_FILE, "r") as f:
                old_status = json.load(f)
        with open(globVar.ERBS_RRU_ALARM_FILE, "w") as f:
            old_status.update(statusDict)
            json.dump(old_status, f, indent=1, sort_keys=True)
        logger.info("updated {file}".format(file=globVar.ERBS_RRU_ALARM_FILE))
    else:
        pass


if __name__ == "__main__":
    alarm_search()
    gen()