import json
import logging
import logging.config
import globVar
import os

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

RRU_STATUS_INIT_TEMP_FILE = globVar.ERBS_RRU_INIT_TEMP_FILE
RRU_STATUS_INIT_TEMP_FILE_LINE_NUM = globVar.TEMP_DIR + os.path.sep + ".rru_status_init_temp_file_line_num"
RRU_STATUS_JSON_FILE = globVar.ERBS_RRU_INIT_FILE

def gen():
    init_tmp_line_num = "0"
    if not os.path.isfile(RRU_STATUS_INIT_TEMP_FILE_LINE_NUM):
        with open(RRU_STATUS_INIT_TEMP_FILE_LINE_NUM,"w") as f:
            f.write(init_tmp_line_num)
        logger.debug("generated {file}".format(file=RRU_STATUS_INIT_TEMP_FILE_LINE_NUM))
    else:
        with open(RRU_STATUS_INIT_TEMP_FILE_LINE_NUM,"r") as f:
            logging.info("read {file}".format(file=RRU_STATUS_INIT_TEMP_FILE_LINE_NUM))
            init_tmp_line_num = f.readline()
        logger.debug("record of erbs_rru_status_init.tmp line number in last time: {num}".format(num=init_tmp_line_num))

    with open(RRU_STATUS_INIT_TEMP_FILE, "r") as f:
        logging.info("read {file}".format(file=RRU_STATUS_INIT_TEMP_FILE))
        lines = f.readlines()
        logger.debug("record of erbs_rru_status_init.tmp line number now: {num}".format(num=len(lines)))

    if len(lines) != int(init_tmp_line_num):
        # Generate erbs_rru_status_init.json
        statusDict = {}
        rru = ads = ops = None
        rruNum = 0
        with open(RRU_STATUS_INIT_TEMP_FILE, "r") as f:
            lines = f.readlines()
        logger.info("read {file}".format(file=RRU_STATUS_INIT_TEMP_FILE))
        for line in lines:
            if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
                rru = line.lstrip().rstrip("\n")
            elif line.lstrip().startswith("[11] administrativeState"):
                ads = line.split(":")[1].lstrip().rstrip("\n")
            elif line.lstrip().startswith("[4] operationalState"):
                ops = line.split(":")[1].lstrip().rstrip("\n")
            else:
                pass
            if rru and ads and ops:
                rruNum += 1
                statusDict[rru] = [{"administrativeState": ads, "operationalState": ops}]
                rru = ads = ops = None
        logger.info("found total erbs RRU number: {rruNum}".format(rruNum=rruNum))

        old_status = {}
        if os.path.isfile(RRU_STATUS_JSON_FILE):
            with open(RRU_STATUS_JSON_FILE, "r") as f:
                old_status = json.load(f)
        with open(RRU_STATUS_JSON_FILE, "w") as f:
            old_status.update(statusDict)
            json.dump(old_status, f, indent=1, sort_keys=True)
        logger.info("updated {file}".format(file=RRU_STATUS_JSON_FILE))

        # update line number this time
        with open(RRU_STATUS_INIT_TEMP_FILE_LINE_NUM,"w") as f:
            f.write(str(len(lines)))
        logger.info("updated {file}".format(file=RRU_STATUS_INIT_TEMP_FILE_LINE_NUM))
    else:
        logger.info("no update in {file}".format(file=RRU_STATUS_INIT_TEMP_FILE))

if __name__ == "__main__":
    gen()