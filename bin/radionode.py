#!/usr/bin/python

import json
import globVar
import logging
import logging.config
import os

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("RadionodeController")
except Exception, e:
    print(e)

RRU_STATUS_FILE = globVar.OUTPUT_DIR + os.path.sep + "radionode_rru_status.json"
RRU_STATUS_INI_TMP_FILE = globVar.TEMP_DIR + os.path.sep + "radionode_rru_status_init.tmp"
RRU_STATUS_INI_JSON_FILE = globVar.INPUT_DIR + os.path.sep + "radionode_rru_status_init.json"
RRU_FDN_TMP_FILE = globVar.TEMP_DIR + os.path.sep + "radionode_rru_fdn.tmp"
RRU_FDN_JSON_FILE = globVar.INPUT_DIR + os.path.sep + "radionode_rru_fdn.json"

class Radionode:
    def __init__(self):
        pass

    def genRruStatus(self):
        """
         radionode_rru_status_init.json
        /opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > radionode_rru_status_init.tmp
        :return:
        """
        # Generate radionode_rru_status_init.tmp
        gen_radinode_rru_status_ini_tmp_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > {file}".format(file=RRU_FDN_TMP_FILE)
        logger.info("run {cmd}".format(cmd=gen_radinode_rru_status_ini_tmp_cmd))
        logger.info("generated {file}".format(file=RRU_STATUS_INI_TMP_FILE))
        # Generate radionode_rru_status_init.json
        statusDict = {}
        rru = ads = ops = None
        rruNum = 0
        with open(RRU_STATUS_INI_TMP_FILE, "r") as f:
            lines = f.readlines()
        logger.info("read {file}".format(file=RRU_STATUS_INI_TMP_FILE))
        for line in lines:
            if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
                rru = line.lstrip().rstrip("\n").replace("SubNetwork=ONRM_ROOT_MO_R","SubNetwork=ONRM_ROOT_MO")
            elif line.lstrip().startswith("[1] administrativeState"):
                ads = line.split(":")[1].lstrip().rstrip("\n")
            elif line.lstrip().startswith("[2] operationalState"):
                ops = line.split(":")[1].lstrip().rstrip("\n")
            else:
                pass
            if rru and ads and ops:
                rruNum += 1
                statusDict[rru] = [{"administrativeState": ads, "operationalState": ops}]
                rru = ads = ops = None
        logger.info("found total radionode RRU number: {rruNum}".format(rruNum=rruNum))
        print(statusDict)

        with open(RRU_STATUS_INI_JSON_FILE, "w") as f1:
            json.dump(statusDict, f1, indent=1, sort_keys=True)
            logger.info("generated radnode_rru_status_init.json")

    def genRruFdnList(self):
        """
        /opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > /tmp/radionode_rru_fdn.tmp
         -> input/radionode_rru_fdn.json
        SubNetwork=ONRM_ROOT_MO
        :return:
        """
        RRU_FDN_TMP_FILE = globVar.TEMP_DIR + os.path.sep + "radionode_rru_fdn.tmp"
        RRU_FDN_JSON_FILE = globVar.INPUT_DIR + os.path.sep + "radionode_rru_fdn.json"
        # Generate radionode_rru_fdn.tmp
        gen_radinode_rru_fnd_tmp_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > {file}".format(file=RRU_FDN_TMP_FILE)
        logger.info("run {cmd}".format(cmd=gen_radinode_rru_fnd_tmp_cmd))
        logger.info("generated {file}".format(file=RRU_FDN_TMP_FILE))
        # Generate radionode_rru_fdn.json
        fdnList = []
        fdn = None
        with open(RRU_FDN_TMP_FILE, "r") as f:
            lines = f.readlines()
        logger.info("read {file}".format(file=RRU_FDN_TMP_FILE))
        for line in lines:
            if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
                fdn = line.lstrip().rstrip("\n").replace("SubNetwork=ONRM_ROOT_MO_R","SubNetwork=ONRM_ROOT_MO")
                fdnList.append(fdn)
            else:
                pass
        logger.info("found total radionode RRU fdn: {rruFdn}".format(rruFdn=len(fdnList)))
        with open(RRU_FDN_JSON_FILE, "w") as f1:
            json.dump(fdnList, f1, indent=1, sort_keys=True)
        logger.info("generated {file}".format(file=RRU_FDN_JSON_FILE))


if __name__ == "__main__":
    radonode = Radionode()
    #radonodeController.genRruFdnList()
