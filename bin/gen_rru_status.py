#!/usr/bin/python
import json
import globVar
import time
import os
import logging
import logging.config
import subprocess

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

ERBS_INIT_JSON_FILE = globVar.ERBS_RRU_INIT_FILE
ERBS_NOFIT_JSON_FILE = globVar.ERBS_RRU_NOTIF_FILE
ERBS_ALARM_JSON_FILE = globVar.ERBS_RRU_ALARM_FILE

RADIONODE_RRU_STATUS_FILE = globVar.RADIONODE_RRU_STATUS_FILE

TIME = time.strftime("%Y%m%d%H%M", time.localtime())
RRU_STATUS_FILE = globVar.OUTPUT_DIR + os.path.sep + "rru_status_" + TIME + ".json"
RRU_STATUS_LATEST_FILE = globVar.OUTPUT_DIR + os.path.sep + "rru_status.latest"

def gen():
    merge = {}
    file_list = [ERBS_INIT_JSON_FILE, ERBS_NOFIT_JSON_FILE, ERBS_ALARM_JSON_FILE, RADIONODE_RRU_STATUS_FILE]
    for file in file_list:
        if os.path.isfile(file):
            try:
                logger.info("read {file}".format(file=file))
                with open(file, "r") as f:
                    content = json.load(f)
                os.remove(file)
                logger.info("remove {file}".format(file=file))
                merge.update(content)
            except Exception,e:
                logger.error(e)

    if os.path.isfile(RRU_STATUS_LATEST_FILE):
        try:
            logger.info("read {file}".format(file=RRU_STATUS_LATEST_FILE))
            with open(RRU_STATUS_LATEST_FILE, "r") as f:
                content = json.load(f)
            merge.update(content)
            os.remove(RRU_STATUS_LATEST_FILE)
            logger.info("remove {file}".format(file=RRU_STATUS_LATEST_FILE))
        except Exception, e:
            logger.error(e)
    try:
        with open(RRU_STATUS_FILE, "w") as f:
            json.dump(merge, f, indent=1, sort_keys=True)
        logger.info("updated {file}".format(file=RRU_STATUS_FILE))
        cmd = "ln -s {sfile} {dfile}".format(sfile=RRU_STATUS_FILE, dfile=RRU_STATUS_LATEST_FILE)
        subprocess.call(cmd, shell=True)
        logging.info("run {cmd}".format(cmd = cmd))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    gen()
