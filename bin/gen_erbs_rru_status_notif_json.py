#!/usr/bin/python

import globVar
import logging
import logging.config
import json
import os
import subprocess

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

RRU_FDN_FILE = globVar.ERBS_FDN_FILE
ERBS_RRU_NOTIF_TEMP_FILE = globVar.ERBS_RRU_NOTIF_TEMP_FILE

def gen():
    notifFile = "/var/opt/ericsson/nms_umts_cms_nead_seg/Notifications.log.*"
    monitorCmd = "cat {notifFile} | grep AuxPlugInUnit | grep administrativeState | egrep -v \"AntennaCtrlDevice|AntennaUnit|DeviceGroup|EcPort|EnergyMeter|IplPort|PimcPort|RdiPort|RiPort\" > {notifTmpFile}"\
                .format(notifFile=notifFile,notifTmpFile=ERBS_RRU_NOTIF_TEMP_FILE)
    logger.info("run {cmd}".format(cmd=monitorCmd))
    subprocess.call(monitorCmd, shell=True)
    logger.info("generated {file}".format(file=ERBS_RRU_NOTIF_TEMP_FILE))
    logger.info("read {file}".format(file=ERBS_RRU_NOTIF_TEMP_FILE))
    with open(RRU_FDN_FILE, "r") as f:
        managed_rru_fdn_list = json.load(f)
    with open(ERBS_RRU_NOTIF_TEMP_FILE, "r") as f:
        lines = f.readlines()

    rru_fdn_list = []
    erbs_name_list = []
    try:
        for i in range(len(lines)):
            try:
                rru_fdn = lines[i].split()[3].rstrip(",")
                rru_ads = lines[i].split()[4]
            except Exception, e:
                logger.error(e)
            if rru_fdn.startswith("SubNetwork=ONRM_ROOT_MO_R") and rru_ads.startswith("administrativeState"):
                if rru_fdn in managed_rru_fdn_list:
                    try:
                        erbs_name = rru_fdn.split("=")[3].split(",")[0]
                    except Exception, e:
                        logger.error(e)
                    if rru_fdn not in rru_fdn_list:
                        rru_fdn_list.append(rru_fdn)
                    if erbs_name not in erbs_name_list:
                        erbs_name_list.append(erbs_name)
                else:
                    # not managed rru
                    pass
            else:
                pass
        logger.info("find total {num} managed erbs rru status need update".format(num=len(erbs_name_list)))
        logger.debug("erbs update list {list}".format(list=erbs_name_list))
        #print(rru_fdn_list, erbs_name_list)
    except Exception, e:
        logger.error(e)

    # Need Update
    # amosmobatch -p 5 -t 5 {file} 'lt all ; get' /home/nmsadm/cadev/temp/notif/amosbatch/
    if len(erbs_name_list) > 0:
        erbs_name_amosbatch_file = globVar.TEMP_DIR + os.path.sep + ".erbs_name_notif_amosbatch_file.txt"
        with open(erbs_name_amosbatch_file, "w") as f:
            f.truncate()
            content = "\n".join(erbs_name_list)
            f.writelines(content)
        logger.info("generated {file}".format(file=erbs_name_amosbatch_file))
        get_rru_status_cli = "amosbatch -p 1 {file} 'lt all; get AuxPluginUnit' {dir}".format(file=erbs_name_amosbatch_file, dir=globVar.NOTIF_AMOSBATCH_TEMP_DIR)
        subprocess.call(get_rru_status_cli, shell=True)
        logger.info("run {cmd}".format(cmd=get_rru_status_cli))

        # parse amosbatch output file to get rru status
        firstHalfOfFdn = restHalfOfFdn = ads = ops = None
        statusDict = {}
        for i in range(len(erbs_name_list)):
            amosLogFile = globVar.NOTIF_AMOSBATCH_TEMP_DIR + os.path.sep + erbs_name_list[i] + ".log"
            try:
                logger.info("read {file}".format(file=amosLogFile))
                with open(amosLogFile, "r") as f:
                    lines = f.readlines()
                for k in range(len(lines)):
                    if "SubNetwork=ONRM_ROOT_MO_R" in lines[k] and lines[k].startswith("Connected to"):
                        # example: SubNetwork=ONRM_ROOT_MO_R,SubNetwork=GZKFLTE01,MeContext=GZKFChangLongCunD-ELH,ManagedElement=1
                        firstHalfOfFdn = lines[k].split()[3].strip("()")
                        #print(firstHalfOfFdn)
                    elif "Equipment=1,AuxPlugInUnit=" in lines[k]:
                        # example: Equipment=1,AuxPlugInUnit=RRU-3
                        restHalfOfFdn = lines[k].split()[1]
                        #print(restHalfOfFdn)
                    elif lines[k].startswith("administrativeState"):
                        ads = lines[k].split()[1]
                        #print(ads)
                    elif lines[k].startswith("operationalState"):
                        ops = lines[k].split()[1]
                        #print(ops)
                    else:
                        pass

                    if firstHalfOfFdn and restHalfOfFdn and ads and ops:
                        #print(firstHalfOfFdn + "," + restHalfOfFdn + "_" + ads + "_" + ops)
                        rru = firstHalfOfFdn + "," + restHalfOfFdn
                        statusDict[rru] = [{"administrativeState": ads, "operationalState": ops}]
                        print(statusDict)
                        restHalfOfFdn = ads = ops = None
                        #print("yes")
                os.remove(amosLogFile)
                logger.debug("notification update status {status}".format(status=statusDict))
            except Exception,e:
                logger.error(e)
        # write status to notification update file
        old_status = {}
        if os.path.isfile(globVar.ERBS_RRU_NOTIF_FILE):
            with open(globVar.ERBS_RRU_NOTIF_FILE, "r") as f:
                old_status = json.load(f)
        with open(globVar.ERBS_RRU_NOTIF_FILE, "w") as f:
            old_status.update(statusDict)
            json.dump(old_status, f, indent=1, sort_keys=True)
        logger.info("updated {file}".format(file=globVar.ERBS_RRU_NOTIF_FILE))
    else:
        pass

if __name__ == "__main__":
    gen()
