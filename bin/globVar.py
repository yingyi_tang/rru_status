#!/usr/bin/env python
import os

# MAIN DIR
# /home/nmsadm/cadev/rru_status
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Directory
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
LIB_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "lib"))
FILTER_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "filter"))
INPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "input"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
MODEL_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "model"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
TEMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "temp"))
# second
NOTIF_TEMP_DIR = os.path.abspath(os.path.join(TEMP_DIR, "notif"))
ALARM_TEMP_DIR = os.path.abspath(os.path.join(TEMP_DIR, "alarm"))
# third
NOTIF_AMOSBATCH_TEMP_DIR = os.path.abspath(os.path.join(NOTIF_TEMP_DIR, "amosbatch"))
ALARM_AMOSBATCH_TEMP_DIR = os.path.abspath(os.path.join(ALARM_TEMP_DIR, "amosbatch"))

# Files
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "rru_status.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.conf"))

RADIONODE_FDN_FILE = os.path.abspath(os.path.join(INPUT_DIR, "radionode_rru_fdn.json"))
RADIONODE_NAME_FILE = os.path.abspath(os.path.join(INPUT_DIR, "radionode_name.json"))
RADIONODE_RRU_STATUS_FILE = os.path.abspath(os.path.join(INPUT_DIR, "radionode_rru_status.json"))
RADIONODE_RRU_STATUS_TEMP_FILE = os.path.abspath(os.path.join(TEMP_DIR, "radionode_rru_status.tmp"))

ERBS_FDN_FILE = os.path.abspath(os.path.join(INPUT_DIR, "erbs_rru_fdn.json"))
ERBS_NAME_FILE = os.path.abspath(os.path.join(INPUT_DIR, "erbs_name.json"))
ERBS_RRU_INIT_TEMP_FILE = os.path.abspath(os.path.join(TEMP_DIR, "erbs_rru_status_init.tmp"))
ERBS_RRU_NOTIF_TEMP_FILE = os.path.abspath(os.path.join(TEMP_DIR, "erbs_rru_status_notif.tmp"))
ERBS_RRU_NOTIF_FILE = os.path.abspath(os.path.join(INPUT_DIR, "erbs_rru_status_notif.json"))
ERBS_RRU_ALARM_FILTER_FILE = os.path.abspath(os.path.join(FILTER_DIR, "alarm_filter"))
ERBS_RRU_ALARM_INFO_FILTER_FILE = os.path.abspath(os.path.join(FILTER_DIR, "alarm_info_filter.json"))
ERBS_RRU_ALARM_FILE = os.path.abspath(os.path.join(INPUT_DIR, "erbs_rru_status_alarm.json"))
ERBS_RRU_INIT_FILE = os.path.abspath(os.path.join(INPUT_DIR, "erbs_rru_status_init.json"))

if __name__ == "__main__":
    print BASE_DIR
    print LOGGER_FILE