#!/usr/bin/python
import json
import globVar
import logging
import logging.config
import os
import subprocess

# USER: nmsadm
# discover ERBS RRU information
# FLOW: cstest -> temp/erbs_rru_fdn.tmp -> input/erbs_rru_fdn.json
# OUTOUT: input/erbs_rru_fdn.json
# run at 7AM every day

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("ErbsController")
except Exception, e:
    print(e)

ERBS_RRU_FDN_FILE = globVar.ERBS_FDN_FILE
ERBS_NAME_FILE = globVar.ERBS_NAME_FILE

RADIONODE_RRU_FDN_FILE = globVar.RADIONODE_FDN_FILE
RADIONODE_NAME_FILE = globVar.RADIONODE_NAME_FILE

LATEST_RRU_STATUS_FILE = globVar.OUTPUT_DIR + os.path.sep + "rru_status.latest"

def syncErbs():
    """
    /opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt AuxPlugInUnit -an administrativeState > erbs_rru_fdn.tmp
     -> input/erbs_rru_fdn.json
    SubNetwork=ONRM_ROOT_MO
    :return:
    """
    RRU_FDN_TMP_FILE = globVar.TEMP_DIR + os.path.sep + "erbs_rru_fdn.tmp"
    # Generate radionode_rru_fdn.tmp
    gen_radinode_rru_fnd_tmp_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt AuxPlugInUnit -an administrativeState > {file}".format(file=RRU_FDN_TMP_FILE)
    logger.info("run {cmd}".format(cmd=gen_radinode_rru_fnd_tmp_cmd))
    subprocess.call(gen_radinode_rru_fnd_tmp_cmd, shell=True)
    logger.info("generated {file}".format(file=RRU_FDN_TMP_FILE))
    # Generate radionode_rru_fdn.json
    fdnList = []
    fdn = None
    nameList = []
    name = None
    with open(RRU_FDN_TMP_FILE, "r") as f:
        lines = f.readlines()
    logger.info("read {file}".format(file=RRU_FDN_TMP_FILE))
    for line in lines:
        if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
            fdn = line.lstrip().rstrip("\n")
            fdnList.append(fdn)
            name = line.split(",")[2].lstrip("MeContext=")
            if name not in nameList:
                nameList.append(line.split(",")[2].lstrip("MeContext="))
        else:
            pass
    logger.info("found total erbs rru fdn: {rruFdn}".format(rruFdn=len(fdnList)))
    logger.info("found total erbs: {erbsname}".format(erbsname=len(nameList)))
    with open(ERBS_RRU_FDN_FILE, "w") as f1:
        json.dump(fdnList, f1, indent=1, sort_keys=True)
    logger.info("generated {file}".format(file=ERBS_RRU_FDN_FILE))
    with open(ERBS_NAME_FILE, "w") as f1:
        json.dump(nameList, f1, indent=1, sort_keys=True)
    logger.info("generated {file}".format(file=ERBS_NAME_FILE))

def syncRadionode():
    RRU_FDN_TMP_FILE = globVar.TEMP_DIR + os.path.sep + "radionode_rru_fdn.tmp"
    # Generate radionode_rru_fdn.tmp
    gen_radinode_rru_fnd_tmp_cmd = "/opt/ericsson/nms_cif_cs/etc/unsupported/bin/cstest -s Seg_masterservice_CS lt FieldReplaceableUnit -an administrativeState operationalState > {file}".format(file=RRU_FDN_TMP_FILE)
    logger.info("run {cmd}".format(cmd=gen_radinode_rru_fnd_tmp_cmd))
    try:
        subprocess.call(gen_radinode_rru_fnd_tmp_cmd, shell=True)
    except Exception,e:
        logger.error(e)
    logger.info("generated {file}".format(file=RRU_FDN_TMP_FILE))
    # Generate radionode_rru_fdn.json
    fdnList = []
    fdn = None
    name = None
    nameList = []
    with open(RRU_FDN_TMP_FILE, "r") as f:
        lines = f.readlines()
    logger.info("read {file}".format(file=RRU_FDN_TMP_FILE))
    for line in lines:
        if line.lstrip().startswith("SubNetwork=ONRM_ROOT_MO_R"):
            fdn = line.lstrip().rstrip("\n")
            fdnList.append(fdn)
            name = line.split(",")[2].lstrip("MeContext=")
            if name not in nameList:
                nameList.append(line.split(",")[2].lstrip("MeContext="))
        else:
            pass
    logger.info("found total radionode rru fdn: {rruFdn}".format(rruFdn=len(fdnList)))
    logger.info("found total radionode: {rdname}".format(rdname=len(nameList)))
    with open(RADIONODE_RRU_FDN_FILE, "w") as f1:
        json.dump(fdnList, f1, indent=1, sort_keys=True)
    logger.info("generated {file}".format(file=RADIONODE_RRU_FDN_FILE))
    with open(RADIONODE_NAME_FILE, "w") as f1:
        json.dump(nameList, f1, indent=1, sort_keys=True)
    logger.info("generated {file}".format(file=RADIONODE_NAME_FILE))

def maintain():
    if os.path.isfile(LATEST_RRU_STATUS_FILE):
        content = {}
        fdn = []
        with open(LATEST_RRU_STATUS_FILE, "r") as f:
            content = json.load(f)
        #print content.keys()

        with open(ERBS_RRU_FDN_FILE,"r") as f:
            erbs_fdn = json.load(f)
        with open(RADIONODE_RRU_FDN_FILE, "r") as f:
            radionode_fdn = json.load(f)
        fdn = erbs_fdn + radionode_fdn
        logger.info("sychronized total rru: {snum}, current record rru: {cnum}".format(snum=len(fdn), cnum=len(content)))
        #add_rru = [x for x in content if x in fdn]
        del_rru = [x for x in content if x not in fdn]
        logger.info("delete {num} rru, list: {del_list}".format(num=len(del_rru), del_list=del_rru))
        for rru in del_rru:
            content.pop(rru)
            logger.info("deleted {rru}".format(rru=rru))

        if len(del_rru) != 0:
            with open(LATEST_RRU_STATUS_FILE, "w") as f:
                json.dump(content, f)
            logger.info("updated {file}".format(file=LATEST_RRU_STATUS_FILE))
    else:
        pass

def house_keeping():
    try:
        cmd = "/bin/find {outputDir}/ -mtime 1 -type f -exec rm {char} \;".format(outputDir=globVar.OUTPUT_DIR, char="{}")
        ret = subprocess.call(cmd, shell=True)
        if ret==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))

        # M
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        if fsize >= 10:
            os.remove(globVar.LOG_FILE)
            logger.info("log file size {size}, remove {file}".format(size=fsize, file=globVar.LOG_FILE))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    syncErbs()
    syncRadionode()
    maintain()
    house_keeping()