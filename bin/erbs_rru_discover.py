#!/usr/bin/python
import erbs

# discover ERBS RRU information
# generate input/erbs_rru_fdn.json from temp/erbs_rru_fdn.tmp
# run at 7AM every day

if __name__ == "__main__":
    er = erbs.Erbs()
    er.genRruFdn()